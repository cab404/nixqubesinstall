#!/bin/bash
test -e /rw/bind-dirs/nix && exit 0

# installing nix
curl https://nixos.org/nix/install | sh
. /home/user/.nix-profile/etc/profile.d/nix.sh

# making all the qubes stuff for persistency
sudo mv /nix /rw/bind-dirs/
sudo mkdir -p /rw/config/qubes-bind-dirs.d
sudo tee /rw/config/qubes-bind-dirs.d/50-nix.conf << EOF
binds+=( '/nix' )
EOF

# doing the mount thing
sudo bash /usr/lib/qubes/init/bind-dirs.sh
